import data from './data.json' assert { type: 'json' };
$.each(data, function(i, item) {
    $(".products").append('<hr class="my-4">'+
    '<h5 class="card-title">' + item.name + '</h4>' +
        '<div class="card" style="width:100%">' +
            '<div class="row" style="padding:20px">' +
                '<div class="col-sm-6">' +
                    '<img class="card-img" src="'+ item.img +'" alt="Card image" style="width:50%">' +
                '</div>' +
                '<div class="col-sm-6">' +
                    '<div class="card-body-right">' +
                        '<p class="card-text">Approx price: <b>Rs '+ item.price +'/-</b></p>' +
                        '<p class="card-text">Product Details: </p>' +
                        '<table class="table">' +
                          '<tr>' +
                            '<td>Dimensions</td>' +
                            '<td>' + item.dimensions + '</td>' +
                          '</tr>' +
                          '<tr>' +
                            '<td>Color</td>' +
                            '<td>' + item.color + '</td>' +
                          '</tr>' +
                          '<tr>' +
                            '<td>Bed Size</td>' +
                            '<td>' + item.bed_size + '</td>' +
                          '</tr>' +
                          '<tr>' +
                            '<td>Finish</td>' +
                            '<td>' + item.finish + '</td>' +
                          '</tr>' +
                          '<tr>' +
                            '<td>Material</td>' +
                            '<td>' + item.material + '</td>' +
                          '</tr>' +
                        '</table>' +
                        '<a href="#" class="btn btn-primary">Get Quote</a>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>');
});
